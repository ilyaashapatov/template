var build = 'build',
  source = 'source',
  templates = source + '/templates',
  assets = build + '/assets';

var config = {
  // build js
  js: {
    source: source + '/js/main.js',
    name: 'main.js',
    dest: assets + '/js',
    watch: source + '/js/**'
  },

  // build css
  css: {
    source: source + '/css/main.styl',
    name: 'main.css',
    dest: assets + '/css',
    watch: source + '/css/**',
    autoprefixer: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera > 10', 'Explorer >= 9']
  },

  // build png sprite
  sprites_png: {
    enable: true,
    tmpl: 'utils/sprite_png-template.handlebars',

    // files for watch
    source: [source + '/sprites_png/*.png', '!' + source + '/sprites_png/*@*.png'],
    watch: source + '/sprites_png/*.png',

    // create mixin config
    mixins: source + '/css/variables',
    nameMixins: 'sprite-mixins.styl',
    prefixMixin: 's-',

    // create sprite config
    nameSprite: 'sprite.png',
    dest: assets + '/images',
    imgPath: '../../build/images/sprite.png',

    // support retina config
    supportRetina: true,
    retinaSource: source + '/sprites_png/*@2x.png',
    retinaNameSprite: 'sprite@2x.png',
    retinaImgPath: '../../build/images/sprite@2x.png'
  },

  // build svg sprite
  sprites_svg: {
    enable: true,
    source: source + '/sprites_svg/*.svg',
    watch: source + '/sprites_svg/*.svg',
    dest: templates + '/includes/'
  },

  // build jade template
  jade: {
    enable: true,
    source: [templates + '/**/*.jade', '!' + templates + '/includes/*', '!' + templates + '/base.jade'],
    watch: [templates + '/**/*.jade'],
    dest: build
  },

  // webserver config
  webserver: {
    enable: true,
    src: 'build',
    options: {
      port: 8001
    }
  }
};

require('./utils/index')(config);