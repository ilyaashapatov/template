module.exports = function (config) {
  var gulp = require('gulp');

  var autoprefixer = require('gulp-autoprefixer'),
    stylus = require('gulp-stylus'),
    cssmin = require('gulp-cssmin'),
    jade = require('gulp-jade'),
    spritesmith = require('gulp.spritesmith'),
    svgstore = require('gulp-svgstore'),
    svgmin = require('gulp-svgmin'),

    webserver = require('gulp-webserver'),
    watch = require('gulp-watch'),
    gulpsync = require('gulp-sync')(gulp),
    gulpif = require('gulp-if'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    requi = require('gulp-requi');

  var errors = require('./errors'),
    debug = false;

// TODO: Добавить поддержку SVG и JADE

// =================== CSS ===================
  gulp.task('css', function () {
    gulp.src(config.css.source)
      .pipe(requi())
      .pipe(
        gulpif(
          /[.]styl$/,
          stylus()
        )
      )
      .on('error', errors)
      .pipe(autoprefixer({ browsers: config.css.autoprefixer }))
      .pipe(
        gulpif(
          !debug,
          cssmin()
        )
      )
      .pipe(concat(config.css.name))
      .pipe(gulp.dest(config.css.dest));
  });


// =================== Scripts ===================
  gulp.task('js', function () {
    gulp.src(config.js.source)
      .pipe(requi())
      .on('error', errors)
      .pipe(
        gulpif(
          !debug,
          uglify()
        )
      )
      .pipe(concat(config.js.name))
      .pipe(gulp.dest(config.js.dest));
  });


// =================== PNG Sprites ===================
  gulp.task('baseSprites', function () {

    var opts = {
      cssTemplate: config.sprites_png.tmpl,
      imgName: config.sprites_png.nameSprite,
      cssName: config.sprites_png.nameMixins,
      imgPath: config.sprites_png.imgPath,
      padding: 0,
      cssFormat: 'stylus',
      cssVarMap: function (sprite) {
        sprite.prefix = config.sprites_png.prefixMixin;
        if (config.sprites_png.supportRetina) {
          sprite.retina = true;
          sprite.retinaImgPath = config.sprites_png.retinaImgPath;
        } else {
          sprite.retina = false;
        }
      }
    };

    var spriteData = gulp.src(config.sprites_png.source);

    spriteData = spriteData.pipe(
      spritesmith(opts)
    );

    spriteData.css.pipe(gulp.dest(config.sprites_png.mixins));
    spriteData.img.pipe(gulp.dest(config.sprites_png.dest));
  });

  gulp.task('retinaSprites', function () {

    var opts = {
      imgName: config.sprites_png.retinaNameSprite,
      imgPath: config.sprites_png.retinaImgPath,
      padding: 0,
      cssName: '.'
    };

    var spriteData = gulp.src(config.sprites_png.retinaSource);

    spriteData = spriteData.pipe(
      spritesmith(opts)
    );

    spriteData.img.pipe(gulp.dest(config.sprites_png.dest));
  });

  var tasksSprites = ['baseSprites'];

  if (config.sprites_png.supportRetina) {
    tasksSprites.push('retinaSprites');
  }

  gulp.task('sprites', tasksSprites);


// =================== SVG Sprites ===================
  gulp.task('svgSprites', function () {
    gulp
      .src(config.sprites_svg.source)
      .pipe(svgmin())
      .pipe(svgstore())
      .pipe(gulp.dest(config.sprites_svg.dest));
  });

// ===================  jade  ===================

  gulp.task('jade', function () {
    gulp.src(config.jade.source)
        .pipe(jade({pretty: true}))
        .on('error', errors)
        .pipe(gulp.dest(config.jade.dest));
  });


// ===================  webserver  ==============
  gulp.task('webserver', function () {
    gulp.src(config.webserver.src)
        .pipe(webserver(config.webserver.options));
  });


// =================== watch ====================
  gulp.task('watch', function () {
    watch(config.js.watch, function () {
      gulp.start('js');
    });

    watch(config.css.watch, function () {
      gulp.start('css');
    });

    watch(config.sprites_svg.watch, function () {
      gulp.start('svgSprites');
      gulp.start('jade');
    });

    watch(config.jade.watch, function () {
      gulp.start('jade');
    });

    if (config.sprites_png.enable) {
      watch(config.sprites_png.watch, function () {
        gulp.start('sprites');
      });
    }

  });

  var tasks = [
    'js',
    'css',
    'svgSprites',
    'jade'
  ];

  if (config.sprites_png.enable) {
    tasks.push('sprites');
  }

  if (config.webserver.enable) {
    tasks.push('webserver');
  }

  gulp.task('default', gulpsync.async(tasks));

  gulp.task('start', function () {
    console.log('[ DEBUG ]< =========================');
    debug = true;
    gulp.start(gulpsync.sync(['default', 'watch']));
  });
};